# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

#Notes on Converting to Kotlin Based Scripts

### Android Templates Lack Android Block "compileOptions" Statement

When creating a new Android app in Intellij or Android Studio, the template supplied seems to lack the Android block "compileOptions", especially if the `build.gradle` is converted to `build.gradle.kts`!!

That's not the whole story though, and I'm not 100% clear on it yet, but a number of dependencies and dependency versions might also need to be updated in the process.

1. Update the project  root `build.gradle` to `build.gradle.kts`, and `settings.gradle` to `settings.gradle.kts`. Also update the `app` version of these files.
2. Add a `buildSrc` folder that contains a buildSrc `build.gradle.kts` and `settings.gradle.kts` files.
3. In the `android` block in  `app/build.gradle.kts` add the following, often missing peice of code (compileOptions):

    ```kotlin
    ...
    android {
       ... 
       # maybe under the deafultConfig section of code!? add the following
       compileOptions {
           setSourceCompatibility(JavaVersion.VERSION_1_8)
           setTargetCompatibility(JavaVersion.VERSION_1_8)
       }
       ...
    }
    ```

4. Also update the Kotlin Gradle plugin version number and Android built tools version number. As @ October 2019 there are "1.3.50" and "3.5.1" respectively. 

    ```kotlin
    ...
    val kotlin_version = "1.3.50"
    ...
    dependencies {
       ...
       classpath ("com.android.tools.build:gradle:3.5.1")
       ...
    }
    ...
    ```

5. And update any other outdated dependencies (the IDE should be of some help in that regard). For example, in `app/build.gradle.kts`, update the `core-ktx` dependency version. Intellij should highlight that a newer version is available:

    ```kotlin
    ...
    dependencies {
       ...
       implementation("androidx.core:core-ktx:1.2.0-beta01")
       ...
    }
    ...
    ```

6. All going well, the Android app should be good to go.
