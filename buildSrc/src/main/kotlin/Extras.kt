@file:Suppress("SpellCheckingInspection", "MemberVisibilityCanBePrivate")

object Vers {
    // This library/app

    const val RECYCLERVIEW_V7 = "28.0.0-rc01"


    // Jetbrains
    const val KOTLIN = "1.3.50"
    const val COROUTINES = "1.3.1" //"1.1.1"

    // Android & AndroidX version updates:
    // see https://dl.google.com/dl/android/maven2/index.html
    const val APPCOMPAT = "1.1.0-rc01" // stable is "1.0.2"
    const val CONSTRAINT_LAYOUT = "2.0.0-beta2" // stable is "1.1.3"
    const val CORE = "1.2.0-alpha03"
    const val PREFERENCES = "1.1.0-rc01" // stable is "1.0.0"
    const val MATERIAL = "1.1.0-alpha09" // stable is "1.0.0"
    const val ESPRESSO_CORE = "3.3.0-alpha02" // stable is "3.2.0"
    const val RUNNER = "1.3.0-alpha02" // stable is "1.2.0"

    // Gradle Plugins
    const val ANDROID_APPLICATION = "3.5.0"
    const val ANDROID_EXTENSIONS = KOTLIN

    // Other Third Party Dependencies
    const val ANKO = "0.10.8"
    const val FUEL = "2.0.1"
    const val NEXUS_STAGING = "0.20.0"
    const val MOCKK = "1.9.3"
    const val MOCKSERVER = "5.3.0"
}

object Android {
    // Build Script Android Block
    const val APPLICATION_ID = "nz.salect.orderpos"
    const val COMPILE_SDK_VERSION = 28
    const val MIN_SDK_VERSION = 19
    const val TARGET_SDK_VERSION = 28
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"
    const val TEST_INSTRUMENT_RUNNER = "androidx.test.runner.AndroidJUnitRunner"
}

object Deps {
    //// Library support for Kotlin coroutines
    // Documentation & Repo: https://github.com/Kotlin/kotlinx.coroutines
    const val COROUTINES_CORE_COMMON = "org.jetbrains.kotlinx:kotlinx-coroutines-core-common:${Vers.COROUTINES}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Vers.COROUTINES}"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Vers.COROUTINES}"

    //// HTTP networking library for Kotlin/Android
    // Documentation: https://fuel.gitbook.io/documentation/
    // Repo: https://github.com/kittinunf/fuel
    const val FUEL = "com.github.kittinunf.fuel:fuel:${Vers.FUEL}"
    const val FUEL_ANDROID = "com.github.kittinunf.fuel:fuel-android:${Vers.FUEL}"

    //// Pure Kotlin mocking framework
    // Documentation: https://mockk.io/
    // Repo: https://github.com/mockk/mockk
    const val MOCKK = "io.mockk:mockk:${Vers.MOCKK}"


    //// An HTTP(S) web server that mocks and records requests and responses
    // Documentation: http://www.mock-server.com/
    // Repo: https://github.com/jamesdbloom/mockserver
    const val MOCKSERVER_NETTY = "org.mock-server:mockserver-netty:${Vers.MOCKSERVER}"

    //// AndroidX
    // Repo: https://dl.google.com/dl/android/maven2/index.html
    // Documnetation: https://developer.android.com/jetpack/androidx
    const val APPCOMPAT = "androidx.appcompat:appcompat:${Vers.APPCOMPAT}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Vers.CONSTRAINT_LAYOUT}"
    const val CORE = "androidx.core:core:${Vers.CORE}"

    //// Android Settings/Preferences
    // Documentation: https://developer.android.com/guide/topics/ui/settings.html
    const val PREFERENCES = "androidx.preference:preference:${Vers.PREFERENCES}"

    //// Android Testing
    // Documentation: https://developer.android.com/training/testing

    //// Android Testing Support Library
    // Documentation: https://developer.android.com/training/testing/espresso/index.html
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Vers.ESPRESSO_CORE}"
    // Documentation: https://developer.android.com/training/testing/junit-runner.html
    const val TEST_RUNNER = "androidx.test:runner:${Vers.RUNNER}"

    //// Android Legacy Libraries i.e. pre-AndroidX i.e. com.google.android...
    //// Android Material Components
    const val MATERIAL = "com.google.android.material:material:${Vers.MATERIAL}"
    const val RECYCLERVIEW_V7 = "com.android.support:recyclerview-v7:${Vers.RECYCLERVIEW_V7}"
    const val LIFECYCLE_EXTENSIONS = "androidx.lifecycle:lifecycle-extensions:2.2.0-alpha02"


    private fun ankoDependency(name: String) = "org.jetbrains.anko:$name:${Vers.ANKO}"
    // Anko commons
    val ANKO_COMMONS = ankoDependency("anko-commons")

    // Anko Design
    val ANKO_DESIGN = ankoDependency("anko-design")
    val ANKO_DESIGN_COROUTINES = ankoDependency("anko-design-coroutines")

    // Anko Layouts
    // - the number here should match the min SDK version!!!
    val ANKO_SDK19 = ankoDependency("anko-sdk19")
    val ANKO_APPCOMPAT_V7 = ankoDependency("anko-appcompat-v7")

    // ConstraintLayout
    val ANKO_CONSTRAINT_LAYOUT = ankoDependency("anko-constraint-layout")

    // Anko Coroutine
    val ANKO_COROUTINES = ankoDependency("anko-coroutines")

    // Anko Coroutines Listeners for Anko Layouts
    // - the number here should match the min SDK version!!!
    val ANKO_SDK19_COROUTINES = ankoDependency("anko-sdk19-coroutines")
    val ANKO_APPCOMPAT_V7_COROUTINES = ankoDependency("anko-appcompat-v7-coroutines")

    // Support-v4 (only Anko Commons)
    val ANKO_SUPPORT_V4_COMMONS = ankoDependency("anko-support-v4-commons")

    // Anko RecyclerView
    val ANKO_RECYCLERVIEW_V7 = ankoDependency("anko-recyclerview-v7")
    val ANKO_RECYCLERVIEW_V7_COROUTINES = ankoDependency("anko-recyclerview-v7-coroutines")
}
