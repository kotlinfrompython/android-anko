plugins{
    id("com.android.application")
    kotlin("android")
    kotlin ("android.extensions")
}

val kotlin_version = "1.3.50"
val anko_version = "0.10.8"

android {
    compileSdkVersion(29)
    buildToolsVersion ("29.0.2")
    defaultConfig {
        applicationId = "com.example.planner"
        minSdkVersion(15)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "0.1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_8)
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    //implementation(fileTree(dir: "libs", include: ["*.jar"]))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.core:core-ktx:1.2.0-beta01")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test:runner:1.3.0-alpha02")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0-alpha02")

    // Anko
    implementation("org.jetbrains.anko:anko-sdk19:0.10.8")
    implementation("org.jetbrains.anko:anko-support-v4:0.10.8")
    implementation("org.jetbrains.anko:anko-appcompat-v7:0.10.8")
}
